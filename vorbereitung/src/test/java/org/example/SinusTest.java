package org.example;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SinusTest {

    @Test
    void fakTest() {
        assertEquals(1, Sinus.fak(0));
        assertEquals(1, Sinus.fak(1));
        assertEquals(2, Sinus.fak(2));
        assertEquals(6, Sinus.fak(3));
        assertEquals(120, Sinus.fak(5));
    }

    @Test
    void sinTest1() {
        assertSin(0);
        assertSin(- 0);
        assertSin(Math.PI * 30.0 / 180.0); // 30 Grad
        assertSin(Math.PI / 2.0);
    }

    @Test
    void sinTest2() {
        assertSin(- Math.PI / 2.0d);
        assertSin(Math.PI / 2.0 * 1.00);
        assertSin(Math.PI);
        assertSin(2 * Math.PI);
        assertSin(3 * Math.PI / 2);
        assertSin(-Math.PI);
        assertSin(100 * Math.PI);
    }

    void assertSin(double value) {
        double soll = Math.sin(value);
        double ist = Sinus.sin(value);
        if (Math.abs(soll - ist) < 0.0000001d) {
            assertTrue(true);
        } else {
            String msg = String.format("Failed for value=%f: %f != %f", value, soll, ist);
            assertTrue(false, msg);
        }
    }
}