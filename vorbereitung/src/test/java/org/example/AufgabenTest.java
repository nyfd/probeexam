package org.example;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AufgabenTest {

    @Test
    void twistBuzz_test() {
        Aufgaben.twistBuzz();
    }

    @Test
    void sudokuChecker_test() {
        int[][] sudoku1 = new int[][]{
            {1, 2, 3, 4, 5, 6, 7, 8, 9},
            {2, 3, 4, 5, 6, 7, 8, 9, 1},
            {3, 4, 5, 6, 7, 8, 9, 1, 2},
            {4, 5, 6, 7, 8, 9, 1, 2, 3},
            {5, 6, 7, 8, 9, 1, 2, 3, 4},
            {6, 7, 8, 9, 1, 2, 3, 4, 5},
            {7, 8, 9, 1, 2, 3, 4, 5, 6},
            {8, 9, 1, 2, 3, 4, 5, 6, 7},
            {9, 1, 2, 3, 4, 5, 6, 7, 8}
        };
        assertTrue(Aufgaben.sudokuChecker(sudoku1));

        int[][] sudoku2 = new int[][]{
            {1, 1, 3, 4, 5, 6, 7, 8, 9}, // 2 mal 1
            {2, 3, 4, 5, 6, 7, 8, 9, 1},
            {3, 4, 5, 6, 7, 8, 9, 1, 2},
            {4, 5, 6, 7, 8, 9, 1, 2, 3},
            {5, 6, 7, 8, 9, 1, 2, 3, 4},
            {6, 7, 8, 9, 1, 2, 3, 4, 5},
            {7, 8, 9, 1, 2, 3, 4, 5, 6},
            {8, 9, 1, 2, 3, 4, 5, 6, 7},
            {9, 1, 2, 3, 4, 5, 6, 7, 8}
        };
        assertFalse(Aufgaben.sudokuChecker(sudoku2));

        int[][] sudoku3 = new int[][]{
            {1, 2, 3, 4, 5, 6, 7, 8, 9},
            {2, 3, 4, 5, 6, 7, 8, 9, 1},
            {3, 4, 5, 6, 7, 8, 9, 1, 2},
            {4, 5, 6, 7, 8, 9, 1, 2, 3},
            {5, 6, 7, 8, 9, 1, 2, 3, 4},
            {6, 7, 8, 9, 1, 2, 3, 4, 5},
            {7, 8, 9, 1, 2, 3, 4, 5, 6},
            {8, 9, 1, 2, 3, 4, 5, 6, 7},
            {9, 1, 2, 3, 4, 5, 7, 7, 8} // 2 mal 7
        };
        assertFalse(Aufgaben.sudokuChecker(sudoku3));

        int[][] sudoku4 = new int[][]{
            {1, 2, 3, 4, 5, 6, 7, 8, 9},
            {2, 3, 4, 5, 6, 7, 8, 9, 1},
            {3, 4, 5, 6, 7, 8, 9, 1, 2},
            {2, 5, 6, 7, 8, 9, 1, 2, 3}, // 2 mal 2
            {5, 6, 7, 8, 9, 1, 2, 3, 4},
            {6, 7, 8, 9, 1, 2, 3, 4, 5},
            {7, 8, 9, 1, 2, 3, 4, 5, 6},
            {8, 9, 1, 2, 3, 4, 5, 6, 7},
            {9, 1, 2, 3, 4, 5, 7, 7, 8} // 2 mal 7
        };
        assertFalse(Aufgaben.sudokuChecker(sudoku4));
    }

    @Test
    void sudokuCheckRow_test() {
        int[] row1 = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        assertTrue(Aufgaben.sudokuCheckRow(row1));

        int[] row2 = new int[]{7, 3, 4, 9, 1, 8, 5, 2, 6};
        assertTrue(Aufgaben.sudokuCheckRow(row2));

        int[] rowFail1 = new int[]{1, 1, 3, 4, 5, 6, 7, 8, 9};
        assertFalse(Aufgaben.sudokuCheckRow(rowFail1));

        int[] rowFail2 = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 8};
        assertFalse(Aufgaben.sudokuCheckRow(rowFail2));

        int[] rowFail3 = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 8};
        assertFalse(Aufgaben.sudokuCheckRow(rowFail3));

        int[] rowFail4 = new int[]{7, 7, 7, 7, 7, 7, 7, 7, 7};
        assertFalse(Aufgaben.sudokuCheckRow(rowFail4));
    }

    @Test
    void sudokuCheckCol_test() {
        int[][] sudoku1 = new int[][]{
            {1, 2, 3, 4, 5, 6, 7, 8, 9},
            {2, 3, 4, 5, 6, 7, 8, 9, 1},
            {3, 4, 5, 6, 7, 8, 9, 1, 2},
            {4, 5, 6, 7, 8, 9, 1, 2, 3},
            {5, 6, 7, 8, 9, 1, 2, 3, 4},
            {6, 7, 8, 9, 1, 2, 3, 4, 5},
            {7, 8, 9, 1, 2, 3, 4, 5, 6},
            {8, 9, 1, 2, 3, 4, 5, 6, 7},
            {9, 1, 2, 3, 4, 5, 6, 7, 8}
        };
        assertTrue(Aufgaben.sudokuCheckColumn(sudoku1, 0));
        assertTrue(Aufgaben.sudokuCheckColumn(sudoku1, 1));
        assertTrue(Aufgaben.sudokuCheckColumn(sudoku1, 8));

        int[][] sudoku2 = new int[][]{
            // 1 col hat 2 mal 1
            {1, 2, 3, 4, 5, 6, 7, 8, 9},
            {2, 3, 4, 5, 6, 7, 8, 9, 1},
            {3, 4, 5, 6, 7, 8, 9, 1, 2},
            {4, 5, 6, 7, 8, 9, 1, 2, 3},
            {5, 6, 7, 8, 9, 1, 2, 3, 4},
            {6, 7, 8, 9, 1, 2, 3, 4, 5},
            {7, 8, 9, 1, 2, 3, 4, 5, 6},
            {8, 9, 1, 2, 3, 4, 5, 6, 7},
            {1, 1, 2, 3, 4, 5, 6, 7, 8}
        };
        assertFalse(Aufgaben.sudokuCheckColumn(sudoku2, 0));
        assertTrue(Aufgaben.sudokuCheckColumn(sudoku2, 1));
        assertTrue(Aufgaben.sudokuCheckColumn(sudoku2, 8));
    }

    @Test
    void anagramChecker_test() {
        assertFalse(Aufgaben.anagramChecker("One", "Two"));

        assertTrue(Aufgaben.anagramChecker("One", "One"));

        assertTrue(Aufgaben.anagramChecker("One", "Eno"));

        assertTrue(Aufgaben.anagramChecker("KOLOB", "LOBOK"));

        assertTrue(Aufgaben.anagramChecker("peach", "cheap"));

        assertFalse(Aufgaben.anagramChecker("peach", "cheapp"));
    }

    @Test
    void median_test() {
        assertEquals(2.5d, Aufgaben.median(new int[]{1, 2, 3, 4}));
        assertEquals(15, Aufgaben.median(new int[]{0, 10, 20, 1000}));

        assertEquals(3.0d, Aufgaben.median(new int[]{1, 2, 3, 4, 5}));
        assertEquals(4.0d, Aufgaben.median(new int[]{1, 2, 3, 4, 5, 700, 900}));

    }
}