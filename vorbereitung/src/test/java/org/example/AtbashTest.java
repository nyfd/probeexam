package org.example;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AtbashTest {

    @Test
    void atbashExchange() {
        assertEquals('Z', Atbash.atbashExchange('A'));
        assertEquals('A', Atbash.atbashExchange('Z'));

        assertEquals('Y', Atbash.atbashExchange('B'));
        assertEquals('B', Atbash.atbashExchange('Y'));

        assertEquals('X', Atbash.atbashExchange('C'));
        assertEquals('C', Atbash.atbashExchange('X'));
    }

    @Test
    void atbash() {
        assertEquals("ABCD", Atbash.atbash("ZYXW"));
        assertEquals("ZYXW", Atbash.atbash("ABCD"));

        assertEquals("DCBA", Atbash.atbash("WXYZ"));
        assertEquals("WXYZ", Atbash.atbash("DCBA"));

        assertEquals("AB CD", Atbash.atbash("ZY XW"));
        assertEquals(" AB CD ", Atbash.atbash(" ZY XW "));

        assertEquals("AB, CD?", Atbash.atbash("ZY, XW?"));

    }
}