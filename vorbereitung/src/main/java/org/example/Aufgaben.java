package org.example;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Aufgaben {
    public static void twistBuzz() {
        for (int i = 1; i <= 1000; i = i + 3) {
            if (i % 16 == 0 && i % 19 == 0) {
                System.out.println("Twistbuzz");
            } else if (i % 16 == 0) {
                System.out.println("Twist");
            } else if (i % 19 == 0) {
                System.out.println("Buzz");
            } else {
                System.out.println(i);
            }
        }
    }

    public static boolean sudokuChecker(int[][] sudoku) {
        for (int i = 0; i < sudoku.length; i++) {
            if (!sudokuCheckRow(sudoku[i]) || !sudokuCheckColumn(sudoku, i)) {
                return false;
            }
        }
        return true;
    }

    public static boolean sudokuCheckRow(int[] row) {
        Set<Integer> foundNumbers = new HashSet<>();
        for (Integer currentNumber : row) {
            if (foundNumbers.contains(currentNumber)) {
                return false;
            } else {
                foundNumbers.add(currentNumber);
            }
        }
        return true;
    }

    public static boolean sudokuCheckColumn(int[][] sudoku, int col) {
        Set<Integer> foundNumbers = new HashSet<>();
        for (int[] row : sudoku) {
            Integer currentNumber = row[col];
            if (foundNumbers.contains(currentNumber)) {
                return false;
            } else {
                foundNumbers.add(currentNumber);
            }
        }
        return true;
    }

    public static boolean anagramChecker(String first, String second) {
        // First check the len - if not equal, then false
        if (first.length() != second.length()) {
            return false;
        }

        // Equal strings are considered anagrams of themselves
        if (first.equals(second)) {
            return true;
        }


        Map<Character, Integer> freqMap1 = getLetterFreq(first.toLowerCase());
        Map<Character, Integer> freqMap2 = getLetterFreq(second.toLowerCase());
        return freqMap1.equals(freqMap2);
    }

    public static Map<Character, Integer> getLetterFreq(String str) {
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < str.length(); i ++) {
            Character c = str.charAt(i);
            if (map.containsKey(c)) {
                map.replace(c, map.get(c) + 1);
            } else {
                map.put(c, 1);
            }
        }
        return map;
    }

    public static double median(int[] list) {
        if (list.length % 2 == 0) {
            int indexRight = list.length / 2;
            int indexLeft = indexRight - 1;
            return (list[indexLeft]  + list[indexRight]) / 2.0d;
        } else {
            return list[list.length / 2];
        }
    }

}
