package org.example;

public class Atbash {


    public static char atbashExchange(char letter) {
        int offset = 'Z' - letter;

        return (char) ('A' + offset);
    }


    public static String atbash(String text) {

        String caps = text.toUpperCase();
        StringBuilder result = new StringBuilder(caps.length());
        for (int i = 0; i < caps.length(); i++) {
            char c = caps.charAt(i);
            if (c >= 'A' && c <= 'Z') {
                result.append(atbashExchange(caps.charAt(i)));
            } else {
                result.append(c);
            }
        }
        return result.toString();
    }
}
