package org.example;

public class Sinus {

    private static final double THRESHOLD = 0.00005;

    public static int fak(int value) {
        // 0! = 1
        if (value == 0) {
            return 1;
        } else {
            return value * fak(value - 1);
        }
    }

    public static double sin(double value) {
        double normalizedValue = normalizeValue(value);
        double result = 0.0;
        double teil;
        int n = 1;
        int sign = 1;

        do {
            teil = sign * Math.pow(normalizedValue, n) / fak(n);
            result += teil;
            sign = -sign;

            n = n + 2;
        } while (Math.abs(teil) > THRESHOLD);

        return result;
    }

    public static double normalizeValue(double value) {
        double result = value % (2 * Math.PI);

        if (Math.abs(result) >= Math.PI) {
            if (result > 0.0) {
                result = Math.PI - result;
            } else {
                result = Math.PI + result;
            }
        }
        return result;
    }
}